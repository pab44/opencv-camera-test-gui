#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#define GL_SILENCE_DEPRECATION
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <GLES2/gl2.h>
#endif
#include <GLFW/glfw3.h> // Will drag system OpenGL headers

#include <opencv2/opencv.hpp>
#include <string>

//OpenCV video camera properties here
//https://docs.opencv.org/4.9.0/d4/d15/group__videoio__flags__base.html#ga023786be1ee68a9105bf2e48c700294d

struct CameraProperty{
    
    bool can_edit;
    float value;
    int cv_prop_enum;
    float min;
    float max;
    std::string name;
};

struct CameraPropertiesContainer{

    //property                   can_edit, value, cv_prop_enum, min, max, name
    CameraProperty brightness = {false,55.0f,cv::CAP_PROP_BRIGHTNESS,1.0f,100.0f,std::string("Brightness")};
    CameraProperty contrast = {false,55.0f,cv::CAP_PROP_CONTRAST,1.0f,100.0f,std::string("Contrast")};
    CameraProperty hue = {false,55.0f,cv::CAP_PROP_HUE,1.0f,100.0f,std::string("Hue")};
    CameraProperty fps_frame_rate = {false,55.0f,cv::CAP_PROP_FPS,1.0f,120.0f,std::string("FPS")};
};

cv::VideoCapture opencv_camera;

//function to initialize camera
bool InitCamera_OpenCV(CameraPropertiesContainer* prop_container_ptr);

//initialize an image from 
void GetFrame_Camera_OpenCV(cv::Mat* image_ptr);

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

static void ChangeCameraPropertyGUI(CameraProperty* prop_ptr)
{
    if(prop_ptr->can_edit)
    {
        ImGui::Text(prop_ptr->name.c_str());
        ImGui::SliderFloat(prop_ptr->name.c_str(), &prop_ptr->value, prop_ptr->min, prop_ptr->max);
        opencv_camera.set(prop_ptr->cv_prop_enum,prop_ptr->value);
    }
}


int main(int argc, char* argv[])
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;

    // Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
    // GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
    // GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only
#endif

    // Create window with graphics context
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", nullptr, nullptr);
    if (window == nullptr)
        return 1;
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    CameraPropertiesContainer camera_props;
    
    if(!InitCamera_OpenCV(&camera_props))
    {
        printf("Failed to initialize camera!\n");
        return -1;
    }

    cv::namedWindow("Display window");

    cv::Mat image;

    bool show_demo_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    while (!glfwWindowShouldClose(window))
    {
        //Input and Logic

        //imgui handle input
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to create a named window.
        {
            static float f = 0.0f;

            ImGui::Begin("Camera Settings!");

            ImGui::Checkbox("Demo Window", &show_demo_window);    

            ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

            //camera settings

            ChangeCameraPropertyGUI(&camera_props.fps_frame_rate);
            ChangeCameraPropertyGUI(&camera_props.brightness);
            ChangeCameraPropertyGUI(&camera_props.contrast);
            ChangeCameraPropertyGUI(&camera_props.hue);

            
            
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
            ImGui::End();
        }

        
        
        

        // Rendering

        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);


        //render
        GetFrame_Camera_OpenCV(&image);

        cv::imshow("Display window", image);

        cv::waitKey(25);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

static void TestCameraPropertyEdit(CameraProperty* prop_ptr)
{
    float value = 10.0f;
    if(opencv_camera.set(prop_ptr->cv_prop_enum,value)) 
    {
        prop_ptr->can_edit = true;
    }
    else
    {
        prop_ptr->can_edit = false;
    }
}

bool InitCamera_OpenCV(CameraPropertiesContainer* prop_container_ptr)
{
    // open the first webcam plugged in the computer
    int deviceID = 0;             // 0 = open default camera
    int apiID = cv::CAP_ANY;      // 0 = autodetect default API

    // open selected camera using selected API
    opencv_camera.open(deviceID, apiID);
    if (!opencv_camera.isOpened()) {
        std::cerr << "ERROR: Could not open camera" << std::endl;
        return false;
    }

    //set camera properties

    //set brightness
    TestCameraPropertyEdit(&prop_container_ptr->brightness);

    //set frame rate 
    TestCameraPropertyEdit(&prop_container_ptr->fps_frame_rate);

    //set contrast of image.
    TestCameraPropertyEdit(&prop_container_ptr->contrast);

    //set hue
    TestCameraPropertyEdit(&prop_container_ptr->hue);
   
    return true;

}

void GetFrame_Camera_OpenCV(cv::Mat* image_ptr)
{
    opencv_camera >> *image_ptr;
}